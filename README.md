# Cypress tutorial

This is a cypress tutorial done in ubuntu. Following steps where executed to setup and use cypress

## Install

The cypress tool needs node and npm to begin with. Install the same before proceeding.

Create a development directory and change to the newly created directory. Make sure you have ownership of the directory by running the below command.

```
$sudo chown -R $USER .
```

Initialize npm and set all default values.
```
npm init
```
The above command should create a package.json. All remaining configurations and dependencies can be added/modifed on the package.json file.

Install cypress

```
@npm install cypress
```