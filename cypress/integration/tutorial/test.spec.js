/// <reference types="Cypress" />

describe('Testing of EA App', () => {
    
    before("Before Test for EA App", () => {
        
        cy.fixture("eauser").as("userCred");

        cy.get("@userCred").then((user) => {
            cy.eaLogin(user.username, user.password);
        });
    })

    it('Login to EA App', () => {

        //Visiting website
        // cy.visit('http://eaapp.somee.com/');
        // cy.contains('Login').click();

        // cy.url().should('include', '/Account/Login')

        // cy.get("@userCred").then((user) => {
        //     cy.get('#UserName').type(user.username);
        //     cy.get('#Password').type(user.password);
        // })
        
        // cy.get('.btn').click();

        cy.contains('Employee List').click();
        
        // cy.get('.table').find('tr').as("rows");

        // cy.get("@rows").then(($row) => {
        //     cy.wrap($row).click({ multiple: true });
        // })

        //Using Wrap
        cy.get('.table').find('tr > td').then(($td) => {
            cy.wrap($td).contains("Prashanth").invoke("wrap").parent().contains("Benefits").click();
        })

        //Expected Basic Benefits
        let basicBenefits = ['Gym', 'Dental', 'Hospital'];
        basicBenefits.forEach((benefit) => {
            cy.get('.table').contains(benefit);
        })

        cy.wrap({ name: 'Arun' }).should('have.property', 'name').and('eq', 'Arun');

        cy.contains('Log off').click();

    });
});