/// <reference types="Cypress" />

describe("Testing EA Site for assertions", () => {
    it('Testing EA Site for assertions', () => {
        cy.visit("http://www.executeautomation.com/site/");
        
        cy.get("[aria-label='jump to slide 2']", {timeout:60000}).should('have.class', 'ls-nav-active');

        cy.get("[aria-label='jump to slide 3']", {timeout:60000}).should(($callback) => {
            expect($callback).have.class('ls-nav-active');
            expect($callback).not.to.be.null;
        });
    });
});