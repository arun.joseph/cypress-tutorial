/// <reference types="Cypress" />

describe("Lambdatest for testing XHR", () => {

    before("Load lambda test", () => {
        cy.visit("https://accounts.lambdatest.com/login");
        cy.fixture("lambdauser").as("user");
    })

    it("test to verify xhr", () => {

        cy.server();
        cy.route({
            method: 'GET',
            url: '/api/user/organization/team'
        }).as('team');

        cy.get("@user").then((user) => {
            cy.get(':nth-child(2) > .form-control').click();
            cy.get(':nth-child(2) > .form-control').type(user.username);
            cy.get('#userpassword').type(user.password, {log: false});
            cy.get('.btn').click();
            
            cy.get("@team").then((xhr) => {
                expect(xhr.status).to.eq(200);
                expect(xhr.response.body.data).to.have.length(1);
                expect(xhr.response.body.data[0]).to.have.property("name", "Arun Thomas Joseph"); 
            });

            cy.getCookie("user_id").should('have.property', 'value', '181169');
        })


    })
})