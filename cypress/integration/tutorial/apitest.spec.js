/// <reference types="Cypress" />

context("API test using fake api server", () => {

    after("delete a post with id 2", () => {
        cy.request({
            method: "DELETE",
            url: "http://localhost:3000/posts/2",
            failOnStatusCode: false
        }).then((res) => {
            expect(res.body).to.be.empty
        })
    })
    
    it("testing GET request on fake api server", () => {
        cy.request("http://localhost:3000/posts/1").its("body").should("have.property", "id");
    });

    it("testing POST request on the fake Api server", () => {
        cy.request({
            method: "POST",
            url: "http://localhost:3000/posts",
            body: {
                "id": 2,
                "title": "Exec Automation",
                "author": "Arun"
            }
        }).then((res) => {
            expect(res.body).has.property("title", "Exec Automation");
        })
    });

});